﻿namespace test_project
{
    class Program
    {
        static void Main(string[] args)
        {
            var version = Utilities.Version.Get();

            Console.WriteLine("Version: " + version);
            Console.WriteLine("");

            var Key1 = Environment.GetEnvironmentVariable("KEY1");
            var Key2 = Environment.GetEnvironmentVariable("KEY2");
            var Key3 = Environment.GetEnvironmentVariable("KEY3");

            if (!string.IsNullOrWhiteSpace(Key1))
            {
                Console.WriteLine("Key 1: " + Key1);
            }
            else
            {
                Console.WriteLine("Key 1: Null");
            }

            if (!string.IsNullOrWhiteSpace(Key2))
            {
                Console.WriteLine("Key 2: " + Key2);
            }
            else
            {
                Console.WriteLine("Key 2: Null");
            }

            if (!string.IsNullOrWhiteSpace(Key3))
            {
                Console.WriteLine("Key 3: " + Key3);
            }
            else
            {
                Console.WriteLine("Key 3: Null");
            }

            // Stop Application From Closing
            var command = "";
            while (!command.Equals("x"))
            {
                command = Console.ReadLine() ?? "";
            };
        }
    }
}