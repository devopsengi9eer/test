﻿using System.Reflection;

namespace test_project.Utilities
{
    public static class Version
    {
        public static string Get()
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            string codeVersion = version != null ? version.ToString() : "";
            codeVersion = codeVersion.Length > 2 ? codeVersion[0..^2] : "Unknown";

            return codeVersion;
        }
    }
}
