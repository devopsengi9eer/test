# Use the public .net build image.
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /source

# Perform the Restore as it's own layer.
COPY test-project/*.sln .
COPY test-project/test-project/*.csproj ./test-project/
RUN dotnet restore

# Run the build in another layer.
COPY test-project/CommonAssemblyInfo.cs .
COPY test-project/test-project/. ./test-project/
RUN dotnet build

# Run dotnet publish
FROM build AS publish
WORKDIR /source/test-project
RUN dotnet publish -c release -o /app --no-restore

# Create the publishable image
FROM mcr.microsoft.com/dotnet/runtime:6.0
WORKDIR /app
COPY --from=publish /app ./
ENTRYPOINT ["dotnet", "test-project.dll"]